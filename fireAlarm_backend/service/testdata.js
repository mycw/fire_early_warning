const mongoose = require('mongoose')
const Testdata = mongoose.model('testdata')


//保存数据
export const addTestdata = async (obj)=>{
    const testdata = new Testdata(obj)
    return await testdata.save()
}

//根据id查询
export const getTestdataById = async (id)=>{
    const testdata =  await Testdata.findById(id)
    return testdata
}
//查询数据
export const allFind = async ()=>{
    const testdata = await Testdata.find()
    return testdata
}
//根据ID修改
export const edit = async (id,obj)=>{
    const testdata = await Testdata.update({"_id":id},obj)
    return "Ok"
}
