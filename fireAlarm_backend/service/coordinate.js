const mongoose = require('mongoose')
const Coordinate = mongoose.model('coordinate')


//保存坐标
export const addCoordinate = async (obj)=>{
    const coordinate = new Coordinate(obj)
    return await coordinate.save()
}

//根据id查询(关联查询)
export const getCoordinateById = async (id)=>{
    const coordinate =  await Coordinate.findById(id)
    return coordinate
}
//查询所有坐标
export const allFind = async ()=>{
    const coordinate = await Coordinate.find({state:"1"})
    return coordinate
}
//根据ID修改
export const edit = async (id,obj)=>{
    const coordinate = await Coordinate.update({"_id":id},obj)
    return "Ok"
}
//根据ID删除
export const remove = async (id,obj)=>{
    const coordinate = await Coordinate.update({"_id":id},obj)
    return "Ok"
}
