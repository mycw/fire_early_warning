const mongoose = require('mongoose')
const Floor = mongoose.model('floor')


//保存楼层
export const addFloor = async (obj)=>{
    const floor = new Floor(obj)
    return await floor.save()
}

//根据id查询
export const getFloorById = async (id)=>{
    const floor =  await Floor.findById(id)
    return floor
}
//查询所有
export const allFind = async ()=>{
    const floor = await Floor.find({"state":1})
    return floor
}
//根据ID修改
export const edit = async (id,obj)=>{

    const floor = await Floor.update({"_id":id},obj)
    return floor
}
//根据ID删除
export const remove = async (id,obj)=>{
    const floor = await Floor.update({"_id":id},obj)
    return floor
}
