const mongoose = require('mongoose')
const User = mongoose.model('user')
//判断登录用户密码是否正确
export const checkPassword = async (username,password)=>{
    let match = false
    const user = await User.findOne({username})
    if(user){
        match = await user.comparePassword(password,user.password)
    }
    return {
        user,match
    }
}

//保存用户
export const addUser = async (userinfo)=>{
    const user = new User(userinfo)
    return await user.save()
}

//根据id查询用户
export const getUserById = async (id)=>{
   const user =  await User.findById('5c6ec4c6909991201ce2978a')
   return user
}
//查询所有
export const allFind = async ()=>{
    const user = await User.find()
    return user
}
//根据ID修改
export const edit = async (id,obj)=>{

    const user = await User.update({"_id":id},obj)
    return user
}
//根据ID删除
export const remove = async ()=>{
    const user = await User.remove()
    return user
}
