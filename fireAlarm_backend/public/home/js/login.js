async function login(){
    const username = document.querySelector("#username").value
    const password = document.querySelector("#password").value
    let response = await fetch("/api/v0/user/login",{ 
        method: "POST", 
        headers: {
        'Content-Type': 'application/json'
        }, 
        body:JSON.stringify({username,password})
    })
    response = await response.json()
    if(response){
        $('.modal-body p').text(response.result)
        $('#loginModal').modal('show')
        $('#loginModal').on('hidden.bs.modal', function () {
            window.location.href="/api/v0/user/index"
        })
    }else {
        window.location.href="/api/v0/user/login_info"
    }
}

$(document).ready(function(){
    $('#loginBtn').click(async function(){
        await login()
    })
})
   
