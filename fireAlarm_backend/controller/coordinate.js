import {controller, post, get} from "../lib/decorator";
import {addCoordinate,getCoordinateById,allFind,edit,remove} from "../service/coordinate"
@controller('/api/v0/coordinate')
export class CoordinateController{
    @post('/addCoordinate')
    async addCoordinate(ctx,next){
        var number = ctx.request.body.number;
        console.log(xy);
        var str = xy.split(',');
        var x=str[0];
        console.log(x);
        var y=str[1];
        var text=str[2];
        var state = 1;
        let obj = {
            x,
            y,
            text,
            state,
        }
        const coordinate = await addCoordinate(obj)
        if(coordinate){
            //await ctx.render('home/')//等待补充页面路径
        }else {
            //await ctx.render('home/')//等待补充页面路径
        }
    }

    @get('/all')
    async index1(ctx,next){
        const coordinate = await allFind()
        ctx.body = coordinate;
        console.log(ctx.body);
        //await ctx.render('home/index',{session:ctx.session});
    }
    @post('/edit')
    async edit(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        console.log(id)
        var xy = ctx.request.body.xy;
        console.log(xy)
        var str = xy.split(',');
        var x=str[0];
        var y=str[1];
        var text=str[2];
        var state = 1;
        let obj = {
            x,
            y,
            text,
            state,
        }
        const result = await edit(id,obj)
    }

    @post('/id')
    async getUserDetail(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        const result = await getCoordinateById(id)
        console.log(result)
    //    await ctx.render('home/index',res.send(result))
    }

    @post('/remove')
    async edit(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        var state = 0;
        let obj = {
            state,
        }
        const result = await edit(id,obj)
    }

}

