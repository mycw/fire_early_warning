import {controller, post, get} from "../lib/decorator";
import {checkPassword,addUser,getUserById,allFind,edit} from "../service/user"
@controller('/api/v0/user')
export class UserController{
    @get('/index')
    async index(ctx,next){
        ctx.state.title='火灾预警系统'
        await ctx.render('home/index',{session:ctx.session})
    }
    @get('/login_info')
    async login_info(ctx,next){
        ctx.state.title='火灾预警系统'
        await ctx.render('home/login')
    }
    @post('/login')
    async login(ctx,next){

        const {username,password} = ctx.request.body

        const result = await checkPassword(username,password)
        ctx.set('Content-Type','application/json')
        if(result.match&&result.user){
            ctx.session.currentUser = result.user
            ctx.body = {result:'登录成功'}
        }else{
            ctx.body = {result:'登录失败'}
        }
    }

    @get('/register_info')
    async register_info(ctx,next){
        ctx.state.title='火灾预警系统'
        await ctx.render('home/register')
    }
    @post('/register')
    async register(ctx,next){
        //ctx.body = ctx.request.body
        const user = await addUser(ctx.request.body)
        if(user){
            await ctx.render('home/login')
        }
    }

    @get('/index1')
    async index1(ctx,next){
        const user = await allFind()
        ctx.set('Content-Type','application/json')
        // const session = ctx.session;

        // ctx.body = {
        //     success: true,
        //     user
        // }// ctx.body相当于给前台返回数据

         // 给session赋值
        // session.userInfo = user

        ctx.body = user;
        console.log(ctx.body);
        // await ctx.render('home/test');//渲染界面
    }

    @post('/edit')
    async edit(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        var username=ctx.request.body.username;
        var email=ctx.request.body.email;
        var sex=ctx.request.body.sex;
        let obj = {
            username,
            email,
            sex,
        }
        const result = await edit(id,obj)
    }

 @post('/id')
    async getUserDetail(ctx,next){
     var mongoose = require('mongoose');
     var id =mongoose.Types.ObjectId(ctx.request.body.id);
     const result = await getUserById(id)
    await ctx.render('home/index')
    }

}

