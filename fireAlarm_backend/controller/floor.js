import {controller, post, get} from "../lib/decorator";
import {addFloor,getFloorById,allFind,edit,remove} from "../service/floor"
import {addUser} from "../service/user";
@controller('/api/v0/floor')
export class FloorController{
    @post('/addFloor')
    async register(ctx,next){
        var number = ctx.request.body.number;
        var floor_id = ctx.request.body.floor_id;
        var room_id = ctx.request.body.room_id;
        var manufacturer = ctx.request.body.manufacturer;
        var modifier = ctx.request.body.modifier;
        var state = 1;
        let obj = {
            number,
            floor_id,
            room_id,
            manufacturer,
            modifier,
            state,
        }
        const user = await addFloor(obj)
    }

    @get('/all')
    async all(ctx,next){
        const floor = await allFind()
        ctx.body = floor;
        console.log(ctx.body);
        //await ctx.render('home/index',{session:ctx.session});
    }
    @post('/edit')
    async edit(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        var number = ctx.request.body.number;
        var floor_id = ctx.request.body.floor_id;
        var room_id = ctx.request.body.room_id;
        var manufacturer = ctx.request.body.manufacturer;
        var modifier = ctx.request.body.modifier;
        var state = 1;
        let obj = {
            number,
            floor_id,
            room_id,
            manufacturer,
            modifier,
            state,
        }
        const result = await edit(id,obj)
    }

    @post('/id')
    async getUserDetail(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        const result = await getFloorById(id)
        console.log(result)
        //    await ctx.render('home/index',res.send(result))
    }
    @post('/remove')
    async remove(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        var state = 0;
        let obj = {
            state,
        }
        const result = await remove(id,obj)
    }

}

