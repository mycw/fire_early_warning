import {controller, post, get} from "../lib/decorator";
import {addTestdata,getTestdataById,allFind,edit,remove} from "../service/testdata"
@controller('/api/v0/testdata')
export class TestdataController{
    @get('/addTestdata')
    async addTestdata(ctx,next){
        console.log(global.shuju);
        var shuju1 = global.shuju;
        console.log(shuju)
        var str = shuju1.split(',');
        var temperature=str[0];
        console.log(temperature)
        var CH4=str[1];
        var CO=str[2];
        var number = str[3];
        let obj = {
            temperature,
            CH4,
            CO,
            number,
        }
        const testdata = await addTestdata(obj)
    }

    @get('/all')
    async all(ctx,next){
        const testdata = await allFind()
        ctx.body = testdata;
        //await ctx.render('home/index',{session:ctx.session});
    }

    @post('/id')
    async getUserDetail(ctx,next){
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId(ctx.request.body.id);
        const result = await getTestdataById(id)
        console.log(result)
        //    await ctx.render('home/index',res.send(result))
    }
    @get('/edit')
    async edit(ctx,next){
        console.log(global.shuju);
        var mongoose = require('mongoose');
        var id =mongoose.Types.ObjectId('5c7d4cfc618807395c1f05b0');
        var shuju1 = global.shuju;
        var str = shuju1.split(',');
        var temperature=str[0];
        var CH4=str[1];
        var CO=str[2];
        var number = str[3];
        let obj = {
            temperature,
            CH4,
            CO,
            number,
        }
        const result = await edit(id,obj)
    }
}

