const {Router} = require('../lib/decorator')
const path = require('path')
module.exports = (app)=>{
    const apiPath = path.resolve(__dirname,'../controller')
    const router = new Router(app,apiPath)
    router.init()
}
