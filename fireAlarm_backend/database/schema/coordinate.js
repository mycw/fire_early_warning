const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CoordinateSchema = new Schema({
    x:{
        type:String,
        required:true//必须输入的字段
    },
    y:{
        type:String,
        required:true,
    },
    text:{
        type:String,
        required:true
    },
    state:{
        type:String,
        required:true
    },
})

mongoose.model('coordinate',CoordinateSchema,'coordinate')