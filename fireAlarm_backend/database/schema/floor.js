const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FloorSchema = new Schema({
    number:{
        type:String,
        required:true//必须输入的字段
    },
    floor_id:{
        type:String,
        required:true,
    },
    room_id:{
        type:String,
        required:true
    },
    manufacturer:{
        type:String,
        required:true
    },
    state:{
        type:String,
        required:true
    },
    modifier:{
        type:String,
        required:true
    },
})

mongoose.model('floor',FloorSchema,'floor')