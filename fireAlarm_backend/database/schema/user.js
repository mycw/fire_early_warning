const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema
// 定义加密密码计算强度
const SALT_WORK_FACTOR = 10

const UserSchema = new Schema({
    username:{
        type:String,
        unique:true,//唯一索引
        required:true//必须输入的字段
    },
    password:{
        type:String,
        required:true,
        maxlength:12,//输入的TextBox 控件中所允许的最大字符数。
        minlength:6//输入的TextBox 控件中所允许的最小字符数。
    },
    email:{
        type:String,
        required:true
    },
    sex:{
        type:String,
        enum:['male','female'],
        default:'male'
    },
    meta:{
        createdAt:{
            type:Date,
            default:Date.now()
        },
        updatedAt:{
            type:Date,
            default:Date.now()
        }
    }
})
//使用pre中间件在用户信息存储前进行密码加密
UserSchema.pre('save',function(next){
    if(this.isNew){
        this.meta.createdAt = this.meta.updatedAt = Date.now()
    }else{
        this.meta.updatedAt = Date.now()
    }
    next()
})
//保存用户信息
UserSchema.pre('save',function(next){
    if(!this.isModified('password')){
        return next()
    }
    //bcrypt加密
    bcrypt.genSalt(SALT_WORK_FACTOR,(err,salt)=>{
        if(err) next()
        bcrypt.hash(this.password,salt,(err,hash)=>{
            if(err) next()
            this.password = hash
            next()
        })
    })
})

UserSchema.methods = {
    comparePassword:function(_password,password){
        return new Promise((resolve,reject)=>{
            bcrypt.compare(_password,password,(err,sname)=>{
                if(err) reject(err)
                else resolve(sname)
            })
        })
        
    }
}

mongoose.model('user',UserSchema,'user') //执行这里会在数据库中创建一个名叫user的collection