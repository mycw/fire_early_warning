const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TestdataSchema = new Schema({
    temperature:{
        type:String,
        required:true//必须输入的字段
    },
    CH4:{
        type:String,
        required:true,
    },
    CO:{
        type:String,
        required:true
    },
    number:{
        type:String,
        required:true
    },
})

mongoose.model('testdata',TestdataSchema,'testdata')